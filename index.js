// What are conditional statements
	// Conditional Statements allows us to control theflow of our program. It allows us to run a statement/instruction if the condition is met or run another seperate instruction if otherwise.


// [SECTION] if, else, elseif and statement

	let numA = -1;

	// if statement
		// Executes a statement if the specified condition is true.
	if(numA < 0 ) {
		console.log("Hello");
	};

	/*
		syntax:
		if (condition/s) {
			statement/instruction
		}

	*/

	console.log(numA < 0);

	//re-assign the value of numA.
	numA = 0;
	// the if statement did not execute because the condition was not met
	if(numA < 0 ) {
		console.log("Hello again if numA is less than 0!");
	};
	// the condition results in to false.
	console.log(numA < 0);

	// we can also use string data type to make conditional statements.
	let city = "new york";
	
	if(city === "new york"){
		console.log("Welcome to new your city");
	}

	console.log(city === "new york")

	// else if Clause 

	/*
		- Executes a statement if the previous conditions are false and if the specified condition is true.
		- The "else if " clause is optional and can be added to capture additional conditions to change the flow of the program.
	*/

	let numB = 1;

	if(numA < 0){
		console.log("Hello");
	} else if(numB > 0) {
		console.log("World");
	}

	// If the if() condition was passed/met and run, the program will no longer evaluate the else if() condition and process will stop.
	 numA = 1;

	if(numA > 0){
		console.log("Hello");
	} else if(numB > 0) {
		console.log("World");
	}


    city = "tokyo";

	if(city === "new york"){
		console.log("Welcome to new your city");
	} else if (city === "tokyo"){
		console.log("Welcome to tokyo japan");
	}

	// else statement

	/*
		- Executes a statement if all condition are false.
		- The "else" statement is also optional and can be added to capture any other result to change the flow of the program.
	*/

	if(numA === 0){ // the condition is false
		console.log("hello");
	} else if (numB === 0) { // the condition is false
		console.log("world");
	} else {
		console.log("again");
	}	

	// else if and else are not stand alone statements.

	// this will result to error
	// else if (numB === 0) { // the condition is false
	// 	console.log("world");
	// } else {
	// 	console.log("again");
	// }	

	// this will result to error
	// else {
	// 	console.log("again");
	// }	

	// if, else if , and else statements with functions.

	/*
		- Most of the times we would use to if, else if and else statement with functions to control the flow of our applications.
		- By including them inside function, we can decide when a certain conditions will be checked instead of executing statements when JS loads.

	*/

	let message = " no message";
	console.log(message);
	
	function determineTyphoonIntensity(windSpeed) {
		// conditions
		if(windSpeed < 30) {
			return "Not a typhoon yet";
		} else if(windSpeed >= 31 && windSpeed <= 61){
			return "Tropical depression detected.";
		} else if(windSpeed >= 62 && windSpeed <= 88) {
			return "Tropical storm detected.";
		} else if(windSpeed >= 89 && windSpeed <= 177) {
			return "Severe Tropical storm.";
		} else {
			return "Typhoon Detected";
		}
	}
	message = determineTyphoonIntensity(85);
	console.warn(message);

// [SECTION] Truthy and Falsy 

	/*
		- In JS a "truthy" valuue is a value that is considered true when encountered in a boolean context.
		-Value are considered true unless defined otherwise.
		- Falsy value:
			1. False
			2. 0
			3. undefined
			4. null
			5. ""
			6. -0
			7. NaN

	*/

// Truthy Examples

	if(true){
		console.log("Truthy");
	}

	if(1){
		console.log("Truthy");
	}

	if([]){
		console.log("Truthy");
	}

	// let name = "France"

	// if(name){
	// 	console.log("Truthy");
	// }

// Falsy Examples

	if(false){
		console.log("Falsy")
	}

	if(0){
		console.log("Falsy")
	}

	if(undefined){
		console.log("Falsy")
	}

	if(null){
		console.log("Falsy")
	}

	if(""){
		console.log("Falsy")
	}

	if(-0){
		console.log("Falsy")
	}

	if(NaN){
		console.log("Falsy")
	}

// [SECTION] Conditional (ternary) Operator

	/*
		syntax:
			(expression) ? ifTrue/Truthy : ifFalse/Falsy;
	
	*/

// Single Statemetn Execution
	let ternaryResult = (1 < 18) ? true : false;
	console.log("Result of ternary of operator" + ternaryResult)

// Multiple statement execution
let name;

// function isOfLegalAge(){
// 	name = "John";
// 	return "You are of the legal age limit";
// }

// function isUnderAge(){
// 	name = "Jane";
// 	return "Your are under the age limit.";
// }

// let age = parseInt(prompt("What is your age"));
// console.log(age);

// let legalAge = (age > 18 ) ? isOfLegalAge() : isUnderAge();

// console.log("Result of ternary operator in functions "+legalAge+ ", " + name);

// [SECTION] Switch statement

	/*
		- the switch statement evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that selected/matching case.
	
		Syntax:
		switch(expression){
			case value:
				statement;
				break;
		}
	*/
	// let day = prompt("What day of the week is it today?").toLowerCase();

	// console.log(day);

	// switch(day){
	// 	case 'monday':console.log("The color of the day is red");
	// 	break;
	// 	case 'tuesday':console.log("The color of the day is orange");
	// 	break;
	// 	case 'wednesday':console.log("The color of the day is yellow");
	// 	break;
	// 	case 'thurday':console.log("The color of the day is green");
	// 	break;
	// 	case 'friday':console.log("The color of the day is blue");
	// 	break;
	// 	case 'saturday':console.log("The color of the day is indigo");
	// 	break;
	// 	case 'sunday':console.log("The color of the day is violet");
	// 	break;
	// 	default: console.log("Please input a valid day.");
	// 	break;
	// }

// [Section] Try-catch-Finally Statemetn

	function showIntensityAlert(windSpeed){
		// try-catch-finally statement
		try{
			alert(determineTyphoonIntensity(windSpeed));
		} catch (error) {
			console.log(typeof error);
			console.warn(error.message);
		} finally {
			alert("Intensity updates will show new alers");
		}

	}

	showIntensityAlert(56);

	console.log("This message will still show since we handled our error properly")